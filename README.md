# README #
Original Author: Adam Landen
Date Authored: September 21, 2014
****************************************************************************************
Open terminal or Putty and SSH into a linux box to run terminal.
Compile the leap.c file after you have navigated to its directory with gcc using 
command gcc -o leap leap.c.  
After successful compilation, run the program in the directory its compiled to.

### What is this repository for? ###

* Quick summary
Programming in C project repository.
* Version
2014.09.21
* [Learn Markdown](https://bitbucket.org/tutorials/markdowndemo)

### How do I get set up? ###

* Summary of set up
* Configuration
* Dependencies
Linux/Mac Terminal and GCC
* How to run tests
Enter different values and see what is considered a leap year and what is not.
* Deployment instructions
Once compiled with GCC, it is ready for deployment.

### Contribution guidelines ###

* Writing tests
* Code review
* Other guidelines

### Who do I talk to? ###

* Repo owner or admin
* Other community or team contact