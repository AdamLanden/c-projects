

#include <stdio.h>

int main (void){
	int year;
	
	printf("Enter a number to test if it is a leap year in history or in the future.\n");
	scanf("%d", &year);
	
	if (year > 0)
	{
	
		if (year % 4 == 0 && year % 100 !=0)
			{
		 printf("The year %i is a leap year\n", year);
				
			}
		
		else 	{
		printf("The year %i is NOT a leap year\n", year);
				}
		
	}
	else 
		printf("Enter a valid year\n");	

}